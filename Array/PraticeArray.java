package com.array;

import java.util.Arrays;

public class PraticeArray {
	
	public ArrayPrint arrayPrint=new ArrayPrint();
	public int[] mergeArray(int arr1[],int arr2[]) {
		int result[]=new int[arr1.length+arr2.length];
		int index=0;
		for(int i=0;i<result.length;i++) {
			if(i<arr1.length) {
				result[i]=arr1[i];
			}else {
				result[i]=arr2[index];
				index++;
			}
		}
		return result;
	}
	public int[] sortArrayAscending(int arr1[],int arr2[]) {
		int result[]=mergeArray(arr1, arr2);
		Arrays.sort(result);
		arrayPrint.print(result);
		return result;
	}
	public int[] sortArrayDescending(int arr1[],int arr2[]) {
		int result[]=mergeArray(arr1, arr2);
		int desArray[]=new int[result.length];
		Arrays.sort(result);
		int index=0;
		for(int i=result.length-1;i>=0;i--) {
			desArray[index]=result[i];
			index++;
		}
		arrayPrint.print(desArray);
		return desArray;
	}
}
